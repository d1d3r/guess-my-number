# Guess my number

# The computer picks a random number between 1 and 100
# The player tries to guess it and the computer lets
# the player know if the guess is too high, too low
# or right on the money

import random

print("\tWelcome to 'Guess My Number'!")
print("\nI'm thinking of a number between 1 and 100.")
print("Try to guess it in three attempts.\n")

# Set the initial values
the_number = random.randint(1,100)
guess = int(input("Take a guess: "))
tries = 1



# Guessing loop
while guess != the_number:

    if tries >= 3:
        break

    elif guess > the_number:
        print("Lower...",the_number)

    else:
        print("Higher...")
    

    guess = int(input("Take a guess:"))
    tries += 1

if guess == the_number:
    print("You guessed it! The number was")

else:
    print("\nYou loose try again!")

input("\n\nPress the enter key to exit.")
